#ifndef INI_CONFIG_READER
#define INI_CONFIG_READER
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>

#include "configReader.h"

class IniConfigReader : public ConfigReader<IniConfigReader>
{
protected:
	bool parse_config(const std::string& filename);
	
private:
	void parse_tree(const boost::property_tree::ptree& pt, std::string key);
};
#endif

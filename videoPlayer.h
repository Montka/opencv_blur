#ifndef VIDEO_PLAYER
#define VIDEO_PLAYER

#include <memory>

#include <opencv2/opencv.hpp>

#include "frameProcessor.h"

class VideoPlayer
{
public:
	static void mouseCallback(int event, int x, int y, int flag, void* data);
	
	bool playVideo(const std::string& filename);
	void setFrameProcessor(FrameProcessor *fpp);
	
private:	
	int _FPS;
	std::unique_ptr<FrameProcessor> _frameProcessorPtr;
};

#endif

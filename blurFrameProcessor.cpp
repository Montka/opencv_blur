#include "frameProcessor.h"

BlurFrameProcessor::BlurFrameProcessor(int maxRadius, int strenght, int degradeRate):
_blurMaxRadius(maxRadius),
_blurStrenght(strenght),
_blurRadiusDegradeRate(degradeRate),
_mouseX(0),
_mouseY(0),
_currentRadius(maxRadius)
{}

void BlurFrameProcessor::processFrame(cv::Mat frame)
{
	if(_currentRadius > 0) {
        auto roi = cv::Rect(_mouseX - _blurMaxRadius, _mouseY - _blurMaxRadius, _blurMaxRadius * 2, _blurMaxRadius * 2);

        auto image_rect = cv::Rect({}, frame.size());

        // Find intersection, i.e. valid crop region
        auto intersection = image_rect & roi;

        cv::Mat blured_frame;
        frame.copyTo(blured_frame);
        cv::Mat mask = cv::Mat::zeros(frame.size(), frame.type());
        cv::Point circleCenter(_mouseX, _mouseY);
        cv::circle(mask, circleCenter, _currentRadius, CV_RGB(255, 255, 255), -1);
        if (intersection.height > 0 && intersection.width > 0) {
            GaussianBlur(blured_frame(intersection), blured_frame(intersection), cv::Size(_blurStrenght, _blurStrenght),
                         cv::BORDER_ISOLATED);

            blured_frame.copyTo(frame, mask);
        }

        _currentRadius -= _blurRadiusDegradeRate;
    }
}


void BlurFrameProcessor::mouseHandler(int event, int x, int y, int flag)
{
	if (event == cv::EVENT_MOUSEMOVE) {
    		_mouseX = x;
    		_mouseY = y;

    		_currentRadius = _blurMaxRadius;
        	//std::cout << "(" << x << ", " << y << ")" << endl;
    	}

}

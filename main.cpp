#include <string>
#include <iostream>

#include "videoPlayer.h"
#include "frameProcessor.h"
#include "iniConfigReader.h"

int main()
{
    VideoPlayer vp;
    IniConfigReader cf;
    if (!cf.loadConfig("../test.ini"))
        return false;
    int r, s, d;
    try{
        r = std::stoi(cf.getKey("blur.radius"));
        s = std::stoi(cf.getKey("blur.strength"));
        d = std::stoi(cf.getKey("blur.degrade_rate"));
    }
    catch (std::invalid_argument )
    {
        std::cout << "invalid values!" << std::endl;
        return 1;
    }

    vp.setFrameProcessor(new BlurFrameProcessor(r, s, d));
    vp.playVideo(cf.getKey("video.file"));

    return 0;
}
#include "videoPlayer.h"

void VideoPlayer::mouseCallback(int event, int x, int y, int flag, void* data)
{
	VideoPlayer* playerInstance = static_cast<VideoPlayer*> (data);
	
	playerInstance->_frameProcessorPtr->mouseHandler(event, x, y, flag);

}

void VideoPlayer::setFrameProcessor(FrameProcessor *fpp)
{
	_frameProcessorPtr.reset(fpp);
}

bool VideoPlayer::playVideo(const std::string& filename)
{
	cv::VideoCapture cap(filename); 
	if (cap.isOpened() == false) {
		std::cout << "Cannot open the video file" << std::endl;
		return false;
	}

	_FPS = (int)cap.get(cv::CAP_PROP_FPS);

	std::string window_name = "Player";
 	cv::namedWindow(window_name, cv::WINDOW_NORMAL); //create a window
	
	void* frameProcessorInstance = static_cast<void*>(this);
	cv::setMouseCallback(window_name, VideoPlayer::mouseCallback, frameProcessorInstance);
	
	while (true){
	
		cv::Mat frame;
		bool bSuccess = cap.read(frame);
		if (bSuccess == false) {
			std::cout << "Found the end of the video" << std::endl;
   			break;
  		}
	
		_frameProcessorPtr->processFrame(frame);
		cv::imshow(window_name, frame);
		if (cv::waitKey(1000 / _FPS) == 27) { // somehow video seems lagged, Why?
			std::cout << "Esc key is pressed by user. Stoppig the video" << std::endl;
   			break;
  		}
	}
	
	return true;	
}

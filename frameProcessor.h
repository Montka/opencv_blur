#ifndef FRAME_PROCESSOR
#define FRAME_PROCESSOR

#include <opencv2/opencv.hpp>


class FrameProcessor
{
public:
    virtual ~FrameProcessor() = default;
	virtual void processFrame(cv::Mat) = 0;
	virtual void mouseHandler(int event, int x, int y, int flag) = 0;
};


class BlurFrameProcessor : public FrameProcessor
{
public:
	BlurFrameProcessor(int maxRadius, int strenght, int degradeRate);
	virtual ~BlurFrameProcessor() = default;

	virtual void processFrame(cv::Mat frame) override;
	virtual void mouseHandler(int event, int x, int y, int flag) override;
	
private:
//settings
	int _blurMaxRadius;
	int _blurStrenght;
	int _blurRadiusDegradeRate;
	
	int _mouseX;
	int _mouseY;
	int _currentRadius;
};

#endif

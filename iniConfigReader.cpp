#include <iostream>

#include "iniConfigReader.h"

bool IniConfigReader::parse_config(const std::string& filename)
{
	namespace pt = boost::property_tree;
  	pt::ptree propTree;
  	try {
  		read_ini(filename, propTree);
	}
	catch(pt::ini_parser::ini_parser_error) {
		return false;
	}
	
	parse_tree(propTree, "");
	
	for (auto it: _keyMap) {
		std::cout << it.first << " : " << it.second << std::endl;
	}
	return true;
}	

void IniConfigReader::parse_tree(const boost::property_tree::ptree& pt,\
				std::string key)
{
  std::string nkey;

  if (!key.empty()){
  	if (!pt.data().empty()) {
  	  	_keyMap.emplace(key, pt.data());
  	}
  	nkey = key + "."; 
  }
  for (auto it : pt){
  	parse_tree(it.second, nkey + it.first);
  }
}


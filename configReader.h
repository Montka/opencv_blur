#ifndef CONFIG_READER
#define CONFIG_READER
#include <string>
#include <map>
//WARNING: Some mad magic below!


/* CRTP used here.  
   accessor struct requred for keeping realisation of parse_config in client class protected
*/

template<typename T>
class ConfigReader
{
private:
    struct accessor : T
    {
        static bool do_parse(T& derived, const std::string& file)
        {
            bool (T::*fn)(const std::string& ) 
               = &accessor::parse_config;
            return (derived.*fn)(file);
        }
    };

public:
	bool loadConfig(const std::string& filename)
	{
		T& underlying = static_cast<T&>(*this);
		return accessor::do_parse(underlying, filename);
	}
	
	std::string getKey(const std::string& key) const
	{
		if(_keyMap.find(key) != _keyMap.end()){
			return _keyMap.at(key);
		}
		else{
			return "";
		}
	}
	
protected:
	std::map<std::string, std::string> _keyMap;
	
private:
	/*Private constructor for base class needed to prevent 
	  declaration of child class with wrong base template specialization
	  Like: 
	  class IniConfigReader : public ConfigReader<IniConfigReader>
	  class XmlConfigReader : public ConfigReader<IniConfigReader> <---!!!
	  
	*/
	ConfigReader() = default;
	friend T;
	
};
#endif 

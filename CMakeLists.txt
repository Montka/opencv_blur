cmake_minimum_required(VERSION 3.14)
project(BlurPlayer)

find_package( OpenCV REQUIRED )

set(CMAKE_CXX_STANDARD 14)

add_executable(BlurPlayer
        blurFrameProcessor.cpp
        configReader.h
        frameProcessor.h
        iniConfigReader.cpp
        iniConfigReader.h
        main.cpp
        videoPlayer.cpp
        videoPlayer.h)
target_link_libraries( BlurPlayer ${OpenCV_LIBS} )